package ss.virovitica.factorynews3.model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import io.realm.RealmObject;
import ss.virovitica.factorynews3.utility.ArticleErrorException;

/**
 * a data container that represents a single article
 */
public class Article extends RealmObject implements Parcelable {
    //members
    private String author;
    private String title;
    private String description;
    private String url;
    private String urlToImage;
    private String publishedAt;
    private String articleBodyHTML;
    public static final Creator<Article> CREATOR = new Creator<Article>() { //for Parcelable
        @Override
        public Article createFromParcel(Parcel in) {return new Article(in);}
        @Override
        public Article[] newArray(int size) {return new Article[size];}
    };

    //constructor
    public Article(){}

    //constructor (for Parcelable)
    protected Article(Parcel in) {
        author = in.readString();
        title = in.readString();
        description = in.readString();
        url = in.readString();
        urlToImage = in.readString();
        publishedAt = in.readString();
        articleBodyHTML = in.readString();
    }

    //getters
    public String getAuthor() {
        return author;
    }
    public String getTitle() {
        return title;
    }
    public String getDescription() {
        return description;
    }

    /**
     * converts the stored String into a URL object and returns it
     * @return a URL object representing the given location of the article
     * @throws ArticleErrorException the URL was not formed properly and could not be parsed
     */
    private URL getUrl() throws ArticleErrorException {
        URL articleURL;
        try{
            articleURL = new URL(url);
        } catch (MalformedURLException e) {
            Log.e("Article", e.toString());
            e.printStackTrace(); //show details of the original problem
            throw new ArticleErrorException("Could not read the article URL");
        }
        return articleURL;
    }

    public String getImageURLString(){
        return urlToImage;
    }

    /**
     * converts the stored String into a Date object and returns it
     * @return a Date object representing the given date of when the article was published
     * @throws ArticleErrorException the date was not formatted as expected and could not be parsed
     */
    public Date getPublishedAt() throws ArticleErrorException {
        //parse the date String
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date date;
        try {
            date = format.parse(publishedAt);
        } catch (ParseException e) {
            Log.e("Article", e.toString());
            e.printStackTrace();
            throw new ArticleErrorException("Could not parse the publishedAt date");
        }
        return date;
    }

    /**
     * creates a String representation of this Article in JSON format
     * @return String representation of this Article in JSON format
     */
    public String serialize(){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }

    //***Parcelable***//

    @Override
    public int describeContents() {return 0;}

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(author);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(url);
        dest.writeString(urlToImage);
        dest.writeString(publishedAt);
        dest.writeString(articleBodyHTML);
    }
}
