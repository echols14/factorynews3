package ss.virovitica.factorynews3.model;

import org.jetbrains.annotations.NotNull;
import javax.inject.Inject;
import javax.inject.Singleton;
import io.realm.Realm;
import io.realm.RealmResults;

@Singleton
public class ArticleDatabase implements IArticleDatabase {
    //members
    private Realm mRealm = Realm.getDefaultInstance(); //open the first instance

    @Inject
    ArticleDatabase(){}

    //functions

    //***IArticleDatabase***//

    @Override
    public Article[] getArticles() {
        Realm realm = Realm.getDefaultInstance();
        final RealmResults<ArticlesStorage> result = realm.where(ArticlesStorage.class).findAll();
        ArticlesStorage storage = result.first();
        realm.close();
        if(storage == null){
            return new Article[0];
        }
        return storage.getArticles();
    }

    @Override
    public void setArticles(final ArticlesStorage articles) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NotNull Realm realm) {
                realm.deleteAll();
                realm.insert(articles);
            }
        });
        realm.close();
    }

    @Override
    public void close() {
        mRealm.close(); //close the first instance
    }
}
