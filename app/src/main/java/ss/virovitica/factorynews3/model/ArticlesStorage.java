package ss.virovitica.factorynews3.model;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * a data container that stores Articles and related data
 * intended for creation by Gson
 */
public class ArticlesStorage extends RealmObject {
    //members
    private String status;
    private String source;
    private String sortBy;
    private RealmList<Article> articles;

    //getters
    public String getStatus() {
        return status;
    }
    public String getSource() {
        return source;
    }
    public String getSortBy() {
        return sortBy;
    }
    public Article[] getArticles() {
        Article[] articlesArray = new Article[articles.size()];
        for (int i = 0; i < articles.size(); i++) {
            articlesArray[i] = articles.get(i);
        }
        return articlesArray;
    }
}
