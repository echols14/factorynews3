package ss.virovitica.factorynews3.model;

public interface IArticleDatabase {
    Article[] getArticles();
    void setArticles(ArticlesStorage articles);
    void close();
}
