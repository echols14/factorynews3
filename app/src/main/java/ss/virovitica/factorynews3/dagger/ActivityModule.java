package ss.virovitica.factorynews3.dagger;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ss.virovitica.factorynews3.main.MainActivity;
import ss.virovitica.factorynews3.pager.PagerActivity;

@Module
abstract class ActivityModule {
    @PerActivity
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity contributeMainActivity();
    @PerActivity
    @ContributesAndroidInjector(modules = PagerModule.class)
    abstract PagerActivity contributePagerActivity();
}
