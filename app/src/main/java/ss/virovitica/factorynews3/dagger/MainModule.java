package ss.virovitica.factorynews3.dagger;

import dagger.Module;
import dagger.Provides;
import ss.virovitica.factorynews3.adapter.ArticleRecyclerAdapter;
import ss.virovitica.factorynews3.main.IMainInteractor;
import ss.virovitica.factorynews3.main.IMainPresenter;
import ss.virovitica.factorynews3.main.IMainView;
import ss.virovitica.factorynews3.main.MainActivity;
import ss.virovitica.factorynews3.main.MainInteractor;
import ss.virovitica.factorynews3.main.MainPresenter;

@Module
class MainModule {
    @Provides @PerActivity
    IMainView provideView(MainActivity activity){return activity;}
    @Provides @PerActivity
    IMainPresenter providePresenter(MainPresenter presenter){return presenter;}
    @Provides @PerActivity
    IMainInteractor provideInteractor(MainInteractor presenter){return presenter;}
    @Provides @PerActivity
    ArticleRecyclerAdapter provideAdapter(){return new ArticleRecyclerAdapter();}
}
