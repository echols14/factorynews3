package ss.virovitica.factorynews3.dagger;

import dagger.Module;
import dagger.Provides;
import ss.virovitica.factorynews3.pager.IPagerInteractor;
import ss.virovitica.factorynews3.pager.IPagerPresenter;
import ss.virovitica.factorynews3.pager.IPagerView;
import ss.virovitica.factorynews3.pager.PagerActivity;
import ss.virovitica.factorynews3.pager.PagerInteractor;
import ss.virovitica.factorynews3.pager.PagerPresenter;

@Module
class PagerModule {
    @Provides @PerActivity
    IPagerView provideView(PagerActivity activity){return activity;}
    @Provides @PerActivity
    IPagerPresenter providePresenter(PagerPresenter presenter){return presenter;}
    @Provides @PerActivity
    IPagerInteractor provideInteractor(PagerInteractor presenter){return presenter;}
}
