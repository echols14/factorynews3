package ss.virovitica.factorynews3.dagger;

import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import ss.virovitica.factorynews3.NewsApp;
import ss.virovitica.factorynews3.model.ArticleDatabase;
import ss.virovitica.factorynews3.model.IArticleDatabase;

@Module
public class AppModule {
    //members
    private final NewsApp mNewsApp;

    //constructor
    public AppModule(NewsApp app){mNewsApp = app;}

    @Provides @Singleton
    NewsApp provideNewsApp(){return mNewsApp;}
    @Provides @Singleton
    MainModule provideMainModule(){return new MainModule();}
    @Provides @Singleton
    PagerModule providePagerModule(){return new PagerModule();}
    @Provides @Singleton
    IArticleDatabase provideDatabase(ArticleDatabase database){return database;}
}
