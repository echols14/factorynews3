package ss.virovitica.factorynews3.dagger;

import java.util.concurrent.TimeUnit;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetModule {
    //members
    private final long mTimeoutSeconds;
    private final String mBaseUrl;

    //constructor
    public NetModule(long timeoutSeconds, String baseUrl){
        mTimeoutSeconds = timeoutSeconds;
        mBaseUrl = baseUrl;
    }

    //functions

    @Provides @Singleton
    HttpLoggingInterceptor provideInterceptor(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }
    @Provides @Singleton
    OkHttpClient provideOkClient(HttpLoggingInterceptor interceptor){
        return new OkHttpClient.Builder().addInterceptor(interceptor)
                .readTimeout(mTimeoutSeconds, TimeUnit.SECONDS).connectTimeout(mTimeoutSeconds, TimeUnit.SECONDS)
                .build();
    }
    @Provides @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient){
        return new Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create()).build(); //uses Gson to interpret the response
    }
}
