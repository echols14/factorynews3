package ss.virovitica.factorynews3.dagger;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ss.virovitica.factorynews3.pager.ArticleFragment;

@Module
abstract class FragmentModule {
    @PerFragment
    @ContributesAndroidInjector(modules = ArticleModule.class)
    abstract ArticleFragment contributeArticleFragment();
}
