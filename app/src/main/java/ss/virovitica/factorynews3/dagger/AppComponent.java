package ss.virovitica.factorynews3.dagger;

import javax.inject.Singleton;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import ss.virovitica.factorynews3.NewsApp;

@Singleton
@Component(modules = {AppModule.class, NetModule.class, AndroidInjectionModule.class, ActivityModule.class, FragmentModule.class})
public interface AppComponent {
    void inject(NewsApp app);
}
