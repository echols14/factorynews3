package ss.virovitica.factorynews3.net;

import javax.inject.Inject;
import javax.inject.Singleton;
import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ss.virovitica.factorynews3.model.ArticlesStorage;

/**
 * singleton class used to handle http communications
 */
@Singleton
public class ApiClient {
    //constants
    //private static final String NEWS_URL_STRING = "https://newsapi.org/v1/articles?source=bbc-news&sortBy=top&apiKey=6946d0c07a1c4555a4186bfcade76398";
    public static final long TIMEOUT_SECONDS = 10;
    public static final String BASE_URL = "https://newsapi.org/";
    private static final String PATH = "v1/articles";

    private static final String SOURCE = "source";
    private static final String SORT_BY = "sortBy";
    private static final String API_KEY = "apiKey";

    private static final String SOURCE_VAL = "bbc-news";
    private static final String SORT_BY_VAL = "top";
    private static final String API_KEY_VAL = "6946d0c07a1c4555a4186bfcade76398";

    //members
    private final Retrofit mRetrofit;

    //constructor
    @Inject
    ApiClient(Retrofit retrofit){
        mRetrofit = retrofit;
    }

    /**
     * builds a client using retrofit, constructs a call, executes the call, and returns the result body
     * @return the data storage container holding the de-serialized response
     */
    public Observable<ArticlesStorage> getAllArticles(){
        //build a client using retrofit, then get an Observable object from it
        return mRetrofit.create(NewsClient.class).getArticlesObservable(SOURCE_VAL, SORT_BY_VAL, API_KEY_VAL);
    }

    /**
     * interface used by Retrofit
     */
    private interface NewsClient {
        @GET(PATH)
        Observable<ArticlesStorage> getArticlesObservable(@Query(SOURCE) String source, @Query(SORT_BY) String sortBy, @Query(API_KEY) String apiKey);
    }
}
