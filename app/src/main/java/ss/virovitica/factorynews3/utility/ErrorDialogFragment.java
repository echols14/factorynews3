package ss.virovitica.factorynews3.utility;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import ss.virovitica.factorynews3.R;

/**
 * an error dialog that pops up when we encounter an error
 */
public class ErrorDialogFragment extends DialogFragment {
    //constants
    public static final String ERROR_DIALOG = "echols.ErrorDialog";
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //set text fields
        builder.setMessage(R.string.error_message)
                .setTitle(R.string.error)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //close the dialog (and go back to the main activity)
                        if(getActivity() != null) {
                            getActivity().finish();
                            System.exit(0);
                        }
                    }
                });
        return builder.create();
    }
}
