package ss.virovitica.factorynews3.utility;

import android.support.annotation.NonNull;

/**
 * a custom exception that is thrown when an ErrorDialog should be shown
 */
public class ArticleErrorException extends Exception {
    //constructor
    public ArticleErrorException(String message) {
        super(message);
    }

    @NonNull
    public String toString(){
        return super.toString();
    }
}
