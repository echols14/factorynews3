package ss.virovitica.factorynews3.utility;

import android.os.Handler;
import javax.inject.Inject;
import javax.inject.Singleton;
import ss.virovitica.factorynews3.main.IMainPresenter;
import ss.virovitica.factorynews3.pager.IPagerPresenter;

@Singleton
public class Refresher {
    //constants
    private static final int REFRESH_RATE_MS = 1000*60*5;

    //members
    private boolean mIsRunning = false;
    private IMainPresenter mMainPresenter;
    private IPagerPresenter mPagerPresenter;
    private Handler mHandler;
    private final Runnable mRefresher = new Runnable() {
        @Override
        public void run() {
            //refresh regularly using a Handler
            getArticles();
            mHandler.postDelayed(mRefresher, Refresher.REFRESH_RATE_MS);
        }
    };

    //constructor
    @Inject
    Refresher(){}

    //functions

    public void start(){
        if(mIsRunning){ //already running, only need to start once
            return;
        }
        mIsRunning = true;
        //refresh regularly using a Handler
        mHandler = new Handler();
        mRefresher.run();
    }

    public void setMainPresenter(IMainPresenter mainPresenter){
        mMainPresenter = mainPresenter;
    }

    public void setPagerPresenter(IPagerPresenter pagerPresenter){
        mPagerPresenter = pagerPresenter;
    }

    private void getArticles(){
        mMainPresenter.getArticles(); //refreshes the database and main
        if(mPagerPresenter != null){
            mPagerPresenter.getArticles();
        }
    }

    public void showLoadingDialog() {
        mMainPresenter.showLoadingDialog();
        if(mPagerPresenter != null){
            mPagerPresenter.showLoadingDialog();
        }
    }

    public void hideLoadingDialog() {
        mMainPresenter.hideLoadingDialog();
        if(mPagerPresenter != null){
            mPagerPresenter.hideLoadingDialog();
        }
    }
}
