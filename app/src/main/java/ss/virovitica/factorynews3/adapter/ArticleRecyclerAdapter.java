package ss.virovitica.factorynews3.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import ss.virovitica.factorynews3.R;
import ss.virovitica.factorynews3.model.Article;

/**
 * an adapter for the RecyclerView which contains Article previews
 */
public class ArticleRecyclerAdapter extends RecyclerView.Adapter<ArticleHolder> {
    //members
    private Article[] mArticles = new Article[0];

    //functions
    /**
     * load Article objects into the Adapter to be displayed
     * @param articles the Articles to be displayed
     */
    public void setArticles(Article[] articles) {
        mArticles = articles;
    }

    @NonNull
    @Override
    public ArticleHolder onCreateViewHolder(@NonNull ViewGroup parent, int index) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.view_preview, parent, false);
        return new ArticleHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleHolder itemHolder, int index) {
        itemHolder.bind(index, mArticles[index]);
    }

    @Override
    public int getItemCount() {
        return mArticles.length;
    }
}
