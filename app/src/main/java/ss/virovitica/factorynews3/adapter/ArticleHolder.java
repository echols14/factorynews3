package ss.virovitica.factorynews3.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import ss.virovitica.factorynews3.R;
import ss.virovitica.factorynews3.model.Article;
import ss.virovitica.factorynews3.pager.PagerActivity;

/**
 * a ViewHolder used by the RecyclerView that shows Article previews
 */
public class ArticleHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    //members
    private int mIndex;
    private final ImageView mImage;
    private final TextView mTitleText;
    private Context mContext;

    //constructor
    ArticleHolder(@NonNull View parent) {
        super(parent);
        //have this listen for clicks on the item it contains
        parent.setOnClickListener(this);
        //get the associated views
        mImage = parent.findViewById(R.id.image_preview);
        mTitleText = parent.findViewById(R.id.title_text);
    }

    //functions
    void bind(int index, Article article){
        mIndex = index;
        mContext = itemView.getContext();
        //set image
        Glide.with(mContext).load(article.getImageURLString()).into(mImage);
        //set text field
        mTitleText.setText(article.getTitle());
    }

    /**
     * when an Article preview is clicked, this opens an PagerActivity for that Article
     * @param view the view being clicked
     */
    @Override
    public void onClick(View view) {
        Intent intent = PagerActivity.newIntent(mContext, mIndex);
        mContext.startActivity(intent);
    }
}
