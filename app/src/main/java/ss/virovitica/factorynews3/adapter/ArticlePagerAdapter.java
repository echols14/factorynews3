package ss.virovitica.factorynews3.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import ss.virovitica.factorynews3.pager.ArticleFragment;
import ss.virovitica.factorynews3.model.Article;

public class ArticlePagerAdapter extends FragmentStatePagerAdapter {
    //members
    private Article[] mArticles = new Article[0];

    //constructor
    public ArticlePagerAdapter(FragmentManager fm){
        super(fm);
    }

    public void setArticles(Article[] articles){
        mArticles = articles;
    }

    @Override
    public Fragment getItem(int position) {
        return ArticleFragment.newInstance(mArticles[position]);
    }

    @Override
    public int getCount() {
        return mArticles.length;
    }
}
