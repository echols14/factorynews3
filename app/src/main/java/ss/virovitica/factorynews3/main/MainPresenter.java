package ss.virovitica.factorynews3.main;

import javax.inject.Inject;
import ss.virovitica.factorynews3.general.Listener;
import ss.virovitica.factorynews3.model.IArticleDatabase;
import ss.virovitica.factorynews3.utility.Refresher;

public class MainPresenter implements IMainPresenter, Listener {
    //members
    private final IMainView mView;
    private final IMainInteractor mInteractor;
    private final IArticleDatabase mDatabase;
    private final Refresher mRefresher;

    //constructor
    @Inject
    MainPresenter(IMainView view, IMainInteractor interactor, IArticleDatabase database, Refresher refresher){
        mView = view;
        mInteractor = interactor;
        mDatabase = database;
        mRefresher = refresher;
        mRefresher.setMainPresenter(this);
    }

    //functions

    //***IMainPresenter***//

    @Override
    public void startRefresh() {
        mRefresher.start();
    }

    @Override
    public void getArticles() {
        mInteractor.getArticles(this);
    }

    @Override
    public void unsubscribe() {
        mInteractor.unsubscribe();
        mRefresher.setMainPresenter(null);
        mDatabase.close();
    }

    @Override
    public void showLoadingDialog() {
        mView.showLoadingDialog();
    }

    @Override
    public void hideLoadingDialog() {
        mView.hideLoadingDialog();
    }

    //***Listener***//

    @Override
    public void onSuccess() {
        mView.setArticles(mDatabase.getArticles());
    }

    @Override
    public void onError() {
        mView.showErrorDialog();
    }
}
