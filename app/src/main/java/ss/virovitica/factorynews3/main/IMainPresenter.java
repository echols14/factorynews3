package ss.virovitica.factorynews3.main;

public interface IMainPresenter {
    void startRefresh();
    void getArticles();
    void unsubscribe();
    void showLoadingDialog();
    void hideLoadingDialog();
}
