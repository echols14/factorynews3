package ss.virovitica.factorynews3.main;

import javax.inject.Inject;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import ss.virovitica.factorynews3.general.Listener;
import ss.virovitica.factorynews3.model.ArticlesStorage;
import ss.virovitica.factorynews3.model.IArticleDatabase;
import ss.virovitica.factorynews3.net.ApiClient;
import ss.virovitica.factorynews3.utility.Refresher;

public class MainInteractor implements IMainInteractor {
    //members
    private CompositeDisposable mDisposable;
    private final ApiClient mClient;
    private final IArticleDatabase mDatabase;
    private final Refresher mRefresher;

    //constructor
    @Inject
    MainInteractor(ApiClient client, IArticleDatabase database, Refresher refresher){
        mClient = client;
        mDatabase = database;
        mRefresher = refresher;
    }

    //functions

    //***IMainInteractor***//

    @Override
    public void getArticles(final Listener listener) {
        mDisposable = new CompositeDisposable();
        mRefresher.showLoadingDialog();
        mDisposable.add(mClient.getAllArticles()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<ArticlesStorage>() {
                    @Override
                    public void onNext(ArticlesStorage articlesStorage) {
                        mDatabase.setArticles(articlesStorage);
                        listener.onSuccess();
                        mRefresher.hideLoadingDialog();
                    }
                    @Override
                    public void onError(Throwable e) {
                        listener.onError();
                    }
                    @Override
                    public void onComplete() {}
                })
        );
    }

    @Override
    public void unsubscribe() {
        if(mDisposable != null && !mDisposable.isDisposed()) {
            mDisposable.dispose();
        }
    }
}
