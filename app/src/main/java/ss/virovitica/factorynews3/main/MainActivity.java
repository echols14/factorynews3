package ss.virovitica.factorynews3.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import javax.inject.Inject;
import dagger.android.AndroidInjection;
import ss.virovitica.factorynews3.R;
import ss.virovitica.factorynews3.adapter.ArticleRecyclerAdapter;
import ss.virovitica.factorynews3.model.Article;
import ss.virovitica.factorynews3.utility.ErrorDialogFragment;
import static ss.virovitica.factorynews3.utility.ErrorDialogFragment.ERROR_DIALOG;

public class MainActivity extends AppCompatActivity implements IMainView {
    //members
    private IMainPresenter mPresenter;
    private ArticleRecyclerAdapter mAdapter;
    private boolean mIsVisible;
    private ProgressDialog mProgressDialog;
    private boolean mDialogIsShown;

    //functions

    @Inject
    public void useDagger(IMainPresenter presenter, ArticleRecyclerAdapter adapter){
        mPresenter = presenter;
        mAdapter = adapter;
    }

    //***AppCompatActivity***//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //set up the RecyclerView
        RecyclerView articlesRecyclerView = findViewById(R.id.news_recyclerView);
        articlesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        articlesRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIsVisible = true;
        mPresenter.startRefresh();
    }

    @Override
    protected void onPause() {
        mIsVisible = false;
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mPresenter.unsubscribe(); //take care of memory leaks
        super.onDestroy();
    }

    public static Intent newIntent(Context context){
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    //***IMainView***//

    @Override
    public void setArticles(Article[] articles) {
        mAdapter.setArticles(articles);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showErrorDialog() {
        ErrorDialogFragment errorDialog = new ErrorDialogFragment();
        errorDialog.show(getSupportFragmentManager(), ERROR_DIALOG);
    }

    @Override
    public void showLoadingDialog() {
        if(mIsVisible) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading_message));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            mDialogIsShown = true;
        }
    }

    @Override
    public void hideLoadingDialog() {
        if(mDialogIsShown && mProgressDialog != null){
            mProgressDialog.hide();
            mProgressDialog = null;
        }
    }
}
