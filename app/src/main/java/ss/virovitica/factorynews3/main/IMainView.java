package ss.virovitica.factorynews3.main;

import ss.virovitica.factorynews3.model.Article;

public interface IMainView {
    void setArticles(Article[] articles);
    void showErrorDialog();
    void showLoadingDialog();
    void hideLoadingDialog();
}
