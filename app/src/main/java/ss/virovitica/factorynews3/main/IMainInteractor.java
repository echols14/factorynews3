package ss.virovitica.factorynews3.main;

import ss.virovitica.factorynews3.general.Listener;

public interface IMainInteractor {
    void getArticles(Listener listener);
    void unsubscribe();
}
