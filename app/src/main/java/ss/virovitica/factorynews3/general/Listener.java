package ss.virovitica.factorynews3.general;

public interface Listener {
    void onSuccess();
    void onError();
}
