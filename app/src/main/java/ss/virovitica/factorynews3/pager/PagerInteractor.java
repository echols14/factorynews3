package ss.virovitica.factorynews3.pager;

import javax.inject.Inject;
import ss.virovitica.factorynews3.general.Listener;

public class PagerInteractor implements IPagerInteractor {
    //constructor
    @Inject
    PagerInteractor(){}

    //functions

    //***IPagerInteractor***///

    @Override
    public void getArticles(final Listener listener) {
        listener.onSuccess();
    }
}
