package ss.virovitica.factorynews3.pager;

public interface IPagerPresenter {
    void getArticles();
    void setTitle(int index);
    void showLoadingDialog();
    void hideLoadingDialog();
    void finish();
}
