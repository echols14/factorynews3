package ss.virovitica.factorynews3.pager;

import ss.virovitica.factorynews3.general.Listener;

public interface IPagerInteractor {
    void getArticles(final Listener listener);
}
