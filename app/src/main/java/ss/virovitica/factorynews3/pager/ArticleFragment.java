package ss.virovitica.factorynews3.pager;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import dagger.android.support.AndroidSupportInjection;
import ss.virovitica.factorynews3.R;
import ss.virovitica.factorynews3.model.Article;
import ss.virovitica.factorynews3.utility.ErrorDialogFragment;
import static ss.virovitica.factorynews3.utility.ErrorDialogFragment.ERROR_DIALOG;

/**
 * fragment which displays a single article in full
 */
public class ArticleFragment extends Fragment {
    //constants
    private static final String KEY_ARTICLE = "ss.virovitica.factorynews3.pager.ArticleFragment.articleParcelKey";
    //members
    private Article mArticle;

    //functions

    //***Fragment***///

    public static ArticleFragment newInstance(Article article){
        ArticleFragment newFragment = new ArticleFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_ARTICLE, article);
        newFragment.setArguments(bundle);
        return newFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        Bundle args = getArguments();
        if(args != null){
            mArticle = args.getParcelable(KEY_ARTICLE);
        }
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_article, container, false);
        //grab the views
        ImageView topImage = v.findViewById(R.id.top_image);
        TextView titleText = v.findViewById(R.id.title_text);
        TextView bodyText = v.findViewById(R.id.body_text);
        if(mArticle != null){
            //load the article image and text
            Glide.with(this).load(mArticle.getImageURLString()).into(topImage);
            titleText.setText(mArticle.getTitle());
            bodyText.setText(mArticle.getDescription());
        }
        else{ //no article to display
            if(getActivity() != null) {
                ErrorDialogFragment errorDialog = new ErrorDialogFragment();
                errorDialog.show(getActivity().getSupportFragmentManager(), ERROR_DIALOG);
            }
        }
        return v;
    }
}
