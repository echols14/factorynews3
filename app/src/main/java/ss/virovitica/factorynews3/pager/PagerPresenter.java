package ss.virovitica.factorynews3.pager;

import javax.inject.Inject;
import ss.virovitica.factorynews3.general.Listener;
import ss.virovitica.factorynews3.model.IArticleDatabase;
import ss.virovitica.factorynews3.utility.Refresher;

public class PagerPresenter implements IPagerPresenter, Listener {
    //members
    private final IPagerView mView;
    private final IPagerInteractor mInteractor;
    private final IArticleDatabase mDatabase;
    private final Refresher mRefresher;

    //constructor
    @Inject
    PagerPresenter(IPagerView view, IPagerInteractor interactor, IArticleDatabase database, Refresher refresher){
        mView = view;
        mInteractor = interactor;
        mDatabase = database;
        mRefresher = refresher;
        mRefresher.setPagerPresenter(this);
    }

    //functions

    //***IPagerPresenter***//

    @Override
    public void getArticles() {
        mInteractor.getArticles(this);
    }

    @Override
    public void setTitle(int index) {
        mView.setTitle(mDatabase.getArticles()[index].getTitle());
    }

    @Override
    public void showLoadingDialog() {
        mView.showLoadingDialog();
    }

    @Override
    public void hideLoadingDialog() {
        mView.hideLoadingDialog();
    }

    @Override
    public void finish() {
        mRefresher.setPagerPresenter(null);
    }

    //***Listener***//

    @Override
    public void onSuccess() {
        mView.setArticles(mDatabase.getArticles());
    }

    @Override
    public void onError() {
        mView.showErrorDialog();
    }
}
