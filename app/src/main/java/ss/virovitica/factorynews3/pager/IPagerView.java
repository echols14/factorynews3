package ss.virovitica.factorynews3.pager;

import ss.virovitica.factorynews3.model.Article;

public interface IPagerView {
    void setArticles(Article[] articles);
    void setTitle(String title);
    void showErrorDialog();
    void showLoadingDialog();
    void hideLoadingDialog();
}
