package ss.virovitica.factorynews3.pager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import javax.inject.Inject;
import dagger.android.AndroidInjection;
import ss.virovitica.factorynews3.R;
import ss.virovitica.factorynews3.adapter.ArticlePagerAdapter;
import ss.virovitica.factorynews3.main.MainActivity;
import ss.virovitica.factorynews3.model.Article;
import ss.virovitica.factorynews3.utility.ErrorDialogFragment;
import static ss.virovitica.factorynews3.utility.ErrorDialogFragment.ERROR_DIALOG;

public class PagerActivity extends AppCompatActivity implements IPagerView {
    //constants
    private static final String EXTRA_ARTICLE_FOCUS_INDEX = "ss.virovitica.factorynews3.pager.PagerActivity.EXTRA_ARTICLE_FOCUS_INDEX";
    private static final String KEY_CURRENT_ARTICLE_INDEX = "ss.virovitica.factorynews3.pager.PagerActivity.KEY_CURRENT_ARTICLE_INDEX";

    //members
    private IPagerPresenter mPresenter;
    private ArticlePagerAdapter mAdapter;
    private ViewPager mPager;
    private int mStartPageIndex = -1;
    private boolean mIsNew;
    private boolean mIsVisible;
    private ProgressDialog mProgressDialog;
    private boolean mDialogIsShown;

    //functions

    @Inject
    public void useDagger(IPagerPresenter presenter){ //TODO: non-dagger version
        mPresenter = presenter;
    }

    //***AppCompatActivity***//

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);
        //set up the toolbar
        Toolbar toolBar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolBar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        //set up the pager
        mPager = findViewById(R.id.article_pager);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrollStateChanged(int i){}
            @Override
            public void onPageScrolled(int i, float v, int i1){}
            @Override
            public void onPageSelected(int i) {
                mPresenter.setTitle(i);
            }
        });
        //set up the adapter
        mAdapter = new ArticlePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mAdapter);
        //read the info from the intent of what article we want to show first
        Intent intent = getIntent();
        if(intent != null){
            mStartPageIndex = intent.getIntExtra(EXTRA_ARTICLE_FOCUS_INDEX, 0);
            intent.removeExtra(EXTRA_ARTICLE_FOCUS_INDEX);
        }
        mIsNew = true;
        mPresenter.getArticles();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIsVisible = true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            startActivity(MainActivity.newIntent(this));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        mIsVisible = false;
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mPresenter.finish();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        int currentArticleIndex = mPager.getCurrentItem();
        outState.putInt(KEY_CURRENT_ARTICLE_INDEX, currentArticleIndex);
        super.onSaveInstanceState(outState);
    }

    public static Intent newIntent(Context context, int articleIndex){
        Intent intent = new Intent(context, PagerActivity.class);
        intent.putExtra(EXTRA_ARTICLE_FOCUS_INDEX, articleIndex);
        return intent;
    }

    //***IPagerView***//

    @Override
    public void setArticles(Article[] articles) {
        mAdapter.setArticles(articles);
        mAdapter.notifyDataSetChanged();
        if(mIsNew) { //if the article was newly created from a click in the MainActivity, select the article clicked
            mPager.setCurrentItem(mStartPageIndex);
            mIsNew = false;
        }
    }

    @Override
    public void setTitle(String title) {
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public void showErrorDialog() {
        ErrorDialogFragment errorDialog = new ErrorDialogFragment();
        errorDialog.show(getSupportFragmentManager(), ERROR_DIALOG);
    }

    @Override
    public void showLoadingDialog() {
        if(mIsVisible) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading_message));
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            mDialogIsShown = true;
        }
    }

    @Override
    public void hideLoadingDialog() {
        if(mDialogIsShown && mProgressDialog != null){
            mProgressDialog.hide();
            mProgressDialog = null;
        }
    }
}
