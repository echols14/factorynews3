package ss.virovitica.factorynews3;

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;
import javax.inject.Inject;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.support.HasSupportFragmentInjector;
import io.realm.Realm;
import ss.virovitica.factorynews3.dagger.AppComponent;
import ss.virovitica.factorynews3.dagger.AppModule;
import ss.virovitica.factorynews3.dagger.DaggerAppComponent;
import ss.virovitica.factorynews3.dagger.NetModule;
import static ss.virovitica.factorynews3.net.ApiClient.BASE_URL;
import static ss.virovitica.factorynews3.net.ApiClient.TIMEOUT_SECONDS;

public class NewsApp extends Application implements HasActivityInjector, HasSupportFragmentInjector {
    //members
    private static AppComponent mAppComponent;
    private DispatchingAndroidInjector<Activity> mDispatchingAndroidActivityInjector;
    private DispatchingAndroidInjector<Fragment> mDispatchingAndroidFragmentInjector;

    @Inject
    void inject(DispatchingAndroidInjector<Activity> dispatchingAndroidActivityInjector, DispatchingAndroidInjector<Fragment> dispatchingAndroidFragmentInjector){
        mDispatchingAndroidActivityInjector = dispatchingAndroidActivityInjector;
        mDispatchingAndroidFragmentInjector = dispatchingAndroidFragmentInjector;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mAppComponent = buildComponent();
        mAppComponent.inject(this);
        Realm.init(this);
    }

    private AppComponent buildComponent(){
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(TIMEOUT_SECONDS, BASE_URL))
                .build();
    }

    public static AppComponent getComponent(){
        return mAppComponent;
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return mDispatchingAndroidActivityInjector;
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return mDispatchingAndroidFragmentInjector;
    }
}
